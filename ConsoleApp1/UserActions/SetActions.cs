﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainTest;
using Assertion;
using NUnit.Framework;
using ReportingUtility;

namespace UserActions
{
    public static class SetActions
    {
        //To Enter the Text
        public static void EnterText(  PropertyName elementProperty, string element, string value )
        {
            if (elementProperty == PropertyName.Id)
                PropertyCollection.Driver.FindElement(By.Id(element)).SendKeys(value);
            if (elementProperty == PropertyName.Name)
                PropertyCollection.Driver.FindElement(By.Name(element)).SendKeys(value);
        }



        //To Click Button
        public static void ClickButton( PropertyName elementProperty, string element)
        {
            if (elementProperty == PropertyName.Id)
                PropertyCollection.Driver.FindElement(By.Id(element)).Click();
            if (elementProperty == PropertyName.Name)
                PropertyCollection.Driver.FindElement(By.Name(element)).Click();
            if (elementProperty == PropertyName.Xpath)
                PropertyCollection.Driver.FindElement(By.XPath(element)).Click();
        }


        
        //To Select DropDown 
        public static void SelectDropDown(PropertyName elementProperty, string element, string value)
        {
            if (elementProperty == PropertyName.Id)
                new SelectElement(PropertyCollection.Driver.FindElement(By.Id(element))).SelectByText(value);
            if (elementProperty == PropertyName.Name)
                new SelectElement(PropertyCollection.Driver.FindElement(By.Name(element))).SelectByText(value);
        }
        //**************Custom Library Methods*****************
        //To type the text using custom library methods
        public static void TypeText(IWebElement element, string strValue)
        {
            element.SendKeys(strValue);
        }

        //To Click the button using Custom control methods.
        public static void ClickBtn(IWebElement element)
        {
            element.Click();
        }

        //To Select an item in the Dropdown list using Custom control methods.
        public static void SelectDDL(IWebElement element, string strValue)
        {
            new SelectElement(element).SelectByValue(strValue);
        }
        //**************************************************************
        //**************Extended Library Methods*****************
        //To type the text using custom library methods

        public static void ExtendedTypeText(this IWebElement element, string strValue)
        {
            if (InitialAssertion.IsAvailable(element)==true)
            {
                element.SendKeys(strValue);
                SeleniumReporting.WriteResults(true,"Type Text");
            }
            else
            {
                SeleniumReporting.WriteResults(false,"Type Text");
            }
            
        }

        //To Click the button using Custom control methods.
        public static void ExtendedClickBtn(this IWebElement element)
        {
            if (InitialAssertion.IsAvailable(element) == true)
            {
                element.Click();
                SeleniumReporting.WriteResults(true,"Click Button");
            }
            else
            {
                SeleniumReporting.WriteResults(false,"Click Button");
            }

        }

        //To Select an item in the Dropdown list using Custom control methods.
        public static void ExtendedSelectDDL(this IWebElement element, string strValue)
        {
            if (InitialAssertion.IsAvailable(element) == true)
            {
                new SelectElement(element).SelectByValue(strValue);
                SeleniumReporting.WriteResults(true,"Select DropDown List");
            }
            else
            {
                SeleniumReporting.WriteResults(false,"Select DropDown List");
            }
            
        }
        //**************************************************************
    }
}
