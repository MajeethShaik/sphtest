﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserActions;
using ExcelUtilities;
using Assertion;
using ReportingUtility;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;

namespace MainTest
{
    [TestFixture]
    class SPHTestClass
    {

        [OneTimeSetUp]
        public void StartReports()
        {

            string reportPath = GetActions.GetFileName("ReportPath", ".html");
            PropertyCollection.HTMLReporter = new ExtentHtmlReporter(reportPath);
            PropertyCollection.HTMLReporter.Configuration().Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Dark;
            PropertyCollection.HTMLReporter.Configuration().ReportName = "Automation Test Status Report";
            PropertyCollection.HTMLReporter.Configuration().DocumentTitle = "SPHReport";

            PropertyCollection.ExtentReports = new ExtentReports();
            PropertyCollection.ExtentReports.AddSystemInfo("Host Name", "SGIT-MASH-NB");
            PropertyCollection.ExtentReports.AddSystemInfo("Environment", "QA");
            PropertyCollection.ExtentReports.AddSystemInfo("UserName", "Majeeth Shaik");
            PropertyCollection.ExtentReports.AttachReporter(PropertyCollection.HTMLReporter);
        }
        //IWebDriver Driver = new ChromeDriver();

        static void Main(string[] args)
        {
            
        }
        
        [SetUp]
        public void Initialize()
        {
            ExcelLibrary.PopulateCollectionTrial(System.Configuration.ConfigurationManager.AppSettings["TestCasePath"], "TestData");


            PropertyCollection.Driver = new ChromeDriver();
            PropertyCollection.Driver.Navigate().GoToUrl(ExcelLibrary.ReadDataTest("SPHUrl", "UserData"));
            PropertyCollection.Driver.Manage().Window.Maximize();
        }

        [Test]
        public void LoginSpH()          
        {
            PropertyCollection.ExtentTest = PropertyCollection.ExtentReports.CreateTest(TestContext.CurrentContext.Test.Name);
            //Initialize LoginPage by calling its reference.
            TopNavigationPage navigationPage = new TopNavigationPage();
            LoginPageObjects loginpage = navigationPage.NavigateUrl();
            LadningPagePageObjects ladning = loginpage.NavigateLoginPage();
            ladning = loginpage.Login();
            //poPage.POsearch();
            //navigationPage.SignOut();
         
        }
        [TearDown]
        public void GetResult()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stackTrace = "<pre>"+TestContext.CurrentContext.Result.StackTrace+"</pre>";
            var errorMessage = TestContext.CurrentContext.Result.Message;
            if (status == NUnit.Framework.Interfaces.TestStatus.Failed)
            {
                PropertyCollection.ExtentTest.Log(Status.Fail, status + errorMessage);
            }

        }
        [OneTimeTearDown]
        public void CloseBrowser()
        {
            //PropertyCollection.Driver.Close();
            SeleniumReporting.WriteResults(true, "Browser Close");
            PropertyCollection.ExtentReports.Flush();
            
        }


    }
}
