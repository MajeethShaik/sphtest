﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainTest;
using UserActions;
using ExcelUtilities;
using Assertion;
using ReportingUtility;
using NUnit.Framework;

namespace PageObjects
{
    class LoginPageObjects
    {
        //Constructor to initialize the elements in this page.
        //ctor double tab will bring constructor

        public LoginPageObjects()
        {
            PageFactory.InitElements(PropertyCollection.Driver, this);
        }
        //FindsBy Attribute

        [FindsBy(How = How.ClassName, Using = "nav-login")]
        public IWebElement LoginLink { get; set; }

        [FindsBy(How = How.Id, Using = "j_username")]
        public IWebElement UserName { get; set; }

        [FindsBy(How = How.Id, Using = "j_password")]
        public IWebElement PassWord { get; set; }

        [FindsBy(How = How.Id, Using = "login")]
        public IWebElement LoginButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(.,'Advertisement')]//following-sibling::button[@id='close-button']")]
        public IWebElement AdvertisementLable { get; set; }


        public LadningPagePageObjects NavigateLoginPage()
        {
            PropertyCollection.ChildTest = PropertyCollection.ExtentTest.CreateNode(GetActions.GetMyMethodName());
            //AdvertisementLable.ExtendedClickBtn();
            LoginLink.ExtendedClickBtn();
            return new LadningPagePageObjects();
        }
        public LadningPagePageObjects Login()
        {
            PropertyCollection.ChildTest = PropertyCollection.ExtentTest.CreateNode(GetActions.GetMyMethodName());
                
                UserName.ExtendedTypeText(ExcelLibrary.ReadDataTest("UserName", "UserData"));
                PassWord.ExtendedTypeText(ExcelLibrary.ReadDataTest("Password", "UserData"));
                LoginButton.ExtendedClickBtn();
                return new LadningPagePageObjects();


        }
    }
}
