﻿using Assertion;
using ExcelUtilities;
using MainTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using ReportingUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserActions;

namespace PageObjects
{
    class TopNavigationPage
    {
        public TopNavigationPage()
        {
            PageFactory.InitElements(PropertyCollection.Driver, this);
        }

        //Attribute
        [FindsBy(How = How.XPath, Using = "//a[text()='Sign Out']")]
        //*Shortcut: Prop double tab
        public IWebElement BtnSignOut { get; set; }


        public LoginPageObjects SignOut()
        {
            PropertyCollection.ChildTest = PropertyCollection.ExtentTest.CreateNode(GetActions.GetMyMethodName());
            try
            {
                BtnSignOut.ExtendedClickBtn();
                Console.WriteLine("Logout Successfully");
                return new LoginPageObjects();
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Logout button not available in the UI");
                //SeleniumReporting.TakeScreenshot("Logout_Failed.png");
                return new LoginPageObjects();
            }
        }

        public LoginPageObjects NavigateUrl()
        {
            PropertyCollection.ChildTest = PropertyCollection.ExtentTest.CreateNode(GetActions.GetMyMethodName());
            try
            {

                //PropertyCollection.Driver = new ChromeDriver();
                //PropertyCollection.Driver.Navigate().GoToUrl(ExcelLibrary.ReadDataTest("GTUrl", "UserData"));
                //PropertyCollection.Driver.Manage().Window.Maximize();
                SeleniumReporting.WriteResults(true, "Browser Launch and Navigate to '" + ExcelLibrary.ReadDataTest("SPHUrl", "UserData") + "'");


                //BtnSignOut.ExtendedClickBtn();
                //Console.WriteLine("Logout Successfully");
                return new LoginPageObjects();
            }
            catch (NoSuchElementException)
            {
                SeleniumReporting.WriteResults(false, "Browser Launch and Navigate to '" + ExcelLibrary.ReadDataTest("SPHUrl", "UserData") + "'");
                return new LoginPageObjects();
            }
        }
    }

}
