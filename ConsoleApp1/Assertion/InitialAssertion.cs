﻿using ExcelUtilities;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainTest;
using System.Drawing.Imaging;
using UserActions;

namespace Assertion
{
    public class InitialAssertion
    {
        public static bool IsAvailable(IWebElement element)
        {
            try
            {
                return element.Displayed;              
            }
            catch(NoSuchElementException)
            {
                return false;
            }           
        }
    }
}
